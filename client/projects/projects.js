import { Meteor } from 'meteor/meteor';
import { Projects } from '/imports/api/projects.js';
import { Messages } from '/imports/api/messages.js';

var projectId;

Router.route('/projects/:_id?', function () {

  projectId = this.params._id;

  currentUser = Meteor.user();

  if(this.params._id === '_all') {
    this.render('projects', {
      data: {
        projects: Projects.find({}),
        debugAll: true
      }
    });
  } else if(projectId) {
    this.render('singleProject', {
      data: {
        project: Projects.findOne({_id: projectId}),
        messages: Messages.find({project_id: projectId}),
        currentUser: currentUser
      }
    });
  } else {
    this.render('projects', {
      data: {
        projects: Projects.find({$or: [{ owners: currentUser.username }, { contractor: currentUser.username }] })
      }
    });
  }

}, {
  name: 'projects'
});

Template.projects.events({
  'click .delete-project'(event) {
    event.preventDefault();
    Projects.remove({_id: event.target.getAttribute('data-project-id')}, function(err, i){
      console.log(err, i);
    });
  }
})


Template.singleProject.onCreated(function() {
  Meteor.subscribe('projects');
});

Template.singleProject.events({
  'submit #newMessageForm'(event) {

    event.preventDefault();

    const target = event.target;

    var data = {
      content: target.content.value,
      username: Template.currentData().currentUser.username,
      project_id: projectId,
      createdAt: new Date() // current time
    }

    Messages.insert(data);

    target.content.value = '';

  },
  'submit #priceForm'(event) {

    event.preventDefault();

    var username = Template.currentData().currentUser.username;
    var price = event.target.price.value;

    var msg = username + ' set the price to $' + price;

    var data = {
      content: msg,
      type: 'system',
      username: username,
      project_id: projectId,
      createdAt: new Date() // current time
    }

    Messages.insert(data);
    Projects.update({_id: projectId}, {$set: {price: price}})

    event.target.price.value = '';

  },
  'click .action-status-change'(event) {
    event.preventDefault();
    var newStatus = event.target.getAttribute('data-status-change');
    var msg = Template.currentData().currentUser.username;
    if(newStatus === 'dispute') {

      var project = Template.currentData().project;
      var involved = project.owners;
      involved.push(project.contractor);

      var moderator = Meteor.users.findOne({username: {$nin: involved}}, {reactive: false});

      console.log(moderator);

      Projects.update({ _id: projectId }, { $addToSet: { mediators: moderator.username } });

      msg += ' called in a moderator. ' + moderator.username + ' was added.';

    } else if(newStatus === 'success') {

      msg += ' accept the work! Another great deal made!';

    } else if(newStatus === 'payment_requested') {
      msg += ' requested payment for his work.';
    }
    var data = {
      content: msg,
      type: 'system',
      username: Template.currentData().currentUser.username,
      project_id: projectId,
      createdAt: new Date() // current time
    }
    Messages.insert(data);
    Projects.update({_id: projectId}, {$set: {status: newStatus}})
  }
});

Template.singleProject.onRendered(function(){
  $('.modal-trigger').leanModal();
  $('.collapsible').collapsible({
    accordion : false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
  });
  $('ul.tabs').tabs();
  $('ul.tabs').tabs('select_tab', 'message');
});

