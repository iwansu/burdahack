import { Meteor } from 'meteor/meteor';

Router.route('/profiles/:username', function () {

  var username = this.params.username;

  this.render('profile', {
    data: {
      profile: Meteor.users.findOne({username: username})
    }
  });

},{
  name: 'profile'
});

Template.profile.onRendered(function(){

  var color = {
    red: ['#f44336', '#ef5350'],
    green: ['#4caf50', '#66bb6a'],
    yellow: ['#ffeb3b', '#fff176'],
    blue: ['#03a9f4', '#29b6f6'],
    grey: ['#eceff1',
            '#cfd8dc',
            '#b0bec5',
            '#90a4ae',
            '#78909c',
            '#607d8b',
            '#546e7a',
            '#455a64',
            '#37474f']
  };

  var data = {
    friendliness: [{
      value: 15,
      color: color.green[0],
      highlight: color.green[1],
      label: "Success"
    },{
      value: 3,
      color: color.yellow[0],
      highlight: color.yellow[1],
      label: "So-So"
    },{
      value: 1,
      color: color.red[0],
      highlight: color.red[1],
      label: "Declined"
    }],
    time: [{
      value: 13,
      color: color.blue[0],
      highlight: color.blue[1],
      label: "On Time"
    },{
      value: 1,
      color: color.red[0],
      highlight: color.red[1],
      label: "Late"
    },{
      value: 3,
      color: color.green[0],
      highlight: color.green[1],
      label: "Early"
    }],
    stack: [{
      value: 16,
      color: color.grey[8],
      highlight: color.grey[9],
      label: 'Python'
    },{
      value: 16,
      color: color.grey[6],
      highlight: color.grey[7],
      label: 'PostgreSQL'
    },{
      value: 8,
      color: color.grey[0],
      highlight: color.grey[1],
      label: 'HTML'
    },{
      value: 10,
      color: color.grey[2],
      highlight: color.grey[3],
      label: 'JS'
    },{
      value: 2,
      color: color.grey[4],
      highlight: color.grey[5],
      label: 'Ruby'
    },{
      value: 1,
      color: color.grey[6],
      highlight: color.grey[7],
      label: 'Wordpress'
    }]
  };

  var options = {
    segmentShowStroke : true,
    segmentStrokeColor : "#fff",
    segmentStrokeWidth : 2,
    percentageInnerCutout : 50, // This is 0 for Pie charts
    animationSteps : 100,
    animationEasing : "easeOutBounce",
    animateRotate : true,
    animateScale : false,
    legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
  };

  var ctx = document.getElementById("friendliness").getContext("2d");
  var myDoughnutChart = new Chart(ctx).Doughnut(data.friendliness,options);
  var ctx = document.getElementById("time").getContext("2d");
  var myDoughnutChart = new Chart(ctx).Doughnut(data.time,options);
  var ctx = document.getElementById("stack").getContext("2d");
  var myDoughnutChart = new Chart(ctx).Doughnut(data.stack,options);


});
