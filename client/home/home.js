Router.route('/', function () {

  Meteor.subscribe('userData');

  this.render('home', {
    data: {
      users: Meteor.users.find({}),
      currentUser: Meteor.user()
    }
  });
});

Template.home.events({
  'submit #searchForm'(event){
    event.preventDefault();
  }
});
