import { Template } from 'meteor/templating';
import { Projects } from '/imports/api/projects.js';
import { Messages } from '/imports/api/messages.js';
import { Meteor } from 'meteor/meteor';

var contractor;

Router.route('/new-project/:username?', function () {

  contractor = this.params.username;

  this.render('newProject', {
    data: { currentUser: Meteor.user(), contractor: contractor }
  });

}, {
  name: 'new-project'
});

Template.newProject.events({
  'submit #newProjectForm'(event) {
    // Prevent default browser form submit
    event.preventDefault();

    const target = event.target;
    const title = target.title.value;

    const cUsername = Template.currentData().currentUser.username;

    // Insert a task into the collection
    Projects.insert({
      title: title,
      mediators: [],
      contractor: contractor,
      status: 'ongoing',
      owners: [ cUsername ],
      createdAt: new Date() // current time
    }, function(err, project_id){
      Messages.insert({
        createdAt: new Date(),
        username: cUsername,
        content: target.content.value,
        project_id: project_id
      });
      Router.go('projects', {_id: project_id});
    });

  },
});
