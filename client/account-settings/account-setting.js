import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';

Router.route('/account-settings/:username?', function () {


  var user = Meteor.user();
  if(this.params.username) {
    user = Meteor.users.findOne({username: this.params.username});
  }

  this.render('accountSettings', {
    data: {
      user: user
    }
  });

}, {
  name: 'account-settings'
});

Template.accountSettings.events({
  'submit #signUpForm'(event) {

    event.preventDefault();

    const form = event.target;

    var user = Template.currentData().user;

    console.log(form.location.value);

    const userData = {
      profile: {
        first_name: form.first_name.value,
        last_name: form.last_name.value,
        about: form.about.value,
        stack: form.stack.value,
        website: form.website.value,
        location: form.location.value
      }
    };

    Meteor.users.update({_id: user._id}, {$set: userData});

    Router.go('profile', user)

  }
});
