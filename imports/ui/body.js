import { Template } from 'meteor/templating';

import './body.html';

Template.registerHelper('dump', function(obj) {
  return JSON.stringify(obj);
});

Template.registerHelper('isIn', function(needle, haystack) {
  return haystack.indexOf(needle) > -1;
});

Template.registerHelper('equals', function(a, b) {
  return a === b;
});

Template.registerHelper('prettyVar', function(x) {
  return x.split('_').join(' ');
});


Template.registerHelper('getResolveOptions', function(project, currentUser) {
  if(project && currentUser) {

    if(project.status === 'success') {
      return [];
    }

    var options = [];

    if(project.contractor === currentUser.username) {
      //contractor
      if(project.status !== 'payment_requested') {
        options.push({
          status: 'payment_requested',
          icon: 'done',
          title: 'All done?',
          text: 'Well if you\'re all done it\'s money time!',
          confirm: {
            label: 'Request Payment',
            color: 'blue'
          }
        });
      }
      options.push({
        status: 'dispute',
        icon: 'not_interested',
        title: 'Call in a moderator',
        text: 'It look like something is wrong. Somebody from the community will help you resolve your issues.',
          confirm: {
            label: 'Add a moderator',
            color: 'red'
          }
      });
    } else if (project.owners.indexOf(currentUser.username) > -1) {
      //owner
      options.push({
        status: 'success',
        icon: 'done_all',
        title: 'Accept project and allow payout',
        text: 'Great! It looks like you\'re happy with the work done!',
          confirm: {
            label: 'Pay Out',
            color: 'green'
          }
      });
      options.push({
        status: 'dispute',
        icon: 'not_interested',
        title: 'Call in a moderator',
        text: 'It look like something is wrong. Somebody from the community will help you resolve your issues.',
          confirm: {
            label: 'Add a moderator',
            color: 'red'
          }
      });
    } else if (project.moderators && project.moderators.indexOf(currentUser.username) < -1) {
      ///moderators
    }

    return options;

  }
});

Template.registerHelper('avatar', function(username) {

    return '/avatars/' + username + '.jpg';

});
