import { Mongo } from 'meteor/mongo';
import { Meteor } from 'meteor/meteor';


export const Projects = new Mongo.Collection('projects');

Projects.schema = new SimpleSchema({
  title: {type: String},
  contractor: {type: String},
  owners: {type: [String]},
  mediators: {type: [String]},
  createdAt: {type: Date}
});
