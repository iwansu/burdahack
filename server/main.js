import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

import '../imports/api/projects.js';
import '../imports/api/messages.js';

Meteor.startup(() => {
  // code to run on server at startup
});
